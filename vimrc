" Vim, not vi
set nocompatible
" basic plugins
filetype plugin indent on
syntax enable

" show existing tab with 2 spaces width
set tabstop=2
" when indenting with '>', use 2 spaces width
set shiftwidth=2
" On pressing tab, insert 2 spaces
set expandtab
" Show line numbers
set number
" Show ruler
set ruler
" Highlight cursor
set cursorline
set cursorcolumn

"colorscheme
color default

"" Searching
set hlsearch
set incsearch
set ignorecase
set smartcase
