# My dotfiles

Nothing really fancy.

These are my dotfiles and I store them here to grab them everytime I format my computer because of distro-hopping.

## Configs

- alacritty
- arandr
- i3
- i3status
- picom
- vim
